export enum TaskStatus {
    TODO = "TODO",
    InProgress = "InProgress",
    Blocked = "Blocked",
    Done = "Done"
}

export enum TaskPriority {
    Trivial = "Trivial",
    Low = "Low",
    Medium = "Medium",
    High = "High",
    Critical = "Critical"
}

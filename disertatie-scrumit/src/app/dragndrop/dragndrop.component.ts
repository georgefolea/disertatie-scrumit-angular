import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { TaskStatus } from '../models/task-status.enum';
import { TaskPriority } from '../models/task-priority.enum';

@Component({
  selector: 'dragndrop',
  templateUrl: './dragndrop.component.html',
  styleUrls: ['./dragndrop.component.scss']
})
export class DragndropComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  todo = ['TASK-1', 'TASK-2', 'TASK-3', 'TASK-4'];

  ngOnInit(): void {
      
  }

  openDialog() {
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '500px',
      data: {task: 'TASK-3', description: 'Do something with your life', status: TaskStatus.TODO,
       estimation: 8, priority: TaskPriority.High, sprint: 'TeamName/Apr.2/2022' }
    });

    // dialogRef.afterClosed().subscribe(() => {
    //   alert('Congratulation! YOU ARE STUPID ;)')
    // });
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

 

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppComponent } from './app.component';
import { DragndropComponent } from './dragndrop/dragndrop.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SprintOptionsComponent } from './sprint-options/sprint-options.component';
import { ActiveViewShellComponent } from './active-view-shell/active-view-shell.component';
import { FooterComponent } from './footer/footer.component';
import { TaskTileComponent } from './task-tile/task-tile.component';
import { BacklogSprintComponent } from './backlog-sprint/backlog-sprint.component';
import { BacklogComponent } from './backlog/backlog.component';
import { HomeComponent } from './home/home.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { DxDataGridModule,  DxBulletModule,  DxTemplateModule,} from 'devextreme-angular';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMenuModule} from '@angular/material/menu';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

@NgModule({
  declarations: [											
    AppComponent,
      DragndropComponent,
      EditDialogComponent,
      SideMenuComponent,
      NavbarComponent,
      SprintOptionsComponent,
      ActiveViewShellComponent,
      FooterComponent,
      TaskTileComponent,
      BacklogSprintComponent,
      BacklogComponent,
      HomeComponent     
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatSidenavModule,
    MatIconModule,
    DxDataGridModule,
    DxBulletModule,
    DxTemplateModule,
    MatDialogModule,
    MatMenuModule,
    MatButtonToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
